//
// Shinobi
// Copyright (C) 2016 Moe Alam, moeiscool
//
//
// # Donate
//
// If you like what I am doing here and want me to continue please consider donating :)
// PayPal : paypal@m03.ca
//
process.on('uncaughtException', function (err) {
    console.error('uncaughtException',err);
});
var fs = require('fs');


s = {
    mainDirectory: __dirname,
    files: {
        videos : {},
        music : {},
    },
    location : {
        videos : __dirname + "/files/"
    }
}

var readFolder = function(directory,subDir){
    fs.readdirSync(directory).forEach(function(item){
        var fileLocation = directory + item
        var webPath = fileLocation.replace(s.location.videos,'')
        var filelStats = fs.lstatSync(fileLocation)
        var fileStats = fs.statSync(fileLocation)
        if(!filelStats.isDirectory()){
            if(!s.files.videos[subDir])s.files.videos[subDir] = []
            s.files.videos[subDir].push({lstat:filelStats,stat:fileStats,webPath:webPath})
        }else{
            readFolder(fileLocation,directory)
        }
    })
}
readFolder(s.location.videos)

var express = require('express');
var app = express();
var http = require('http');
var bodyParser = require('body-parser');
var server = http.createServer(app);

server.listen(80,function(){
    console.log('CDN on Port 80');
});
app.use(bodyParser.urlencoded({extended: true}));
app.set('views', s.mainDirectory + '/web');
app.set('view engine','ejs');
app.enable('trust proxy');
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});
app.use('/',express.static(s.mainDirectory + '/files'));
app.get('/', function(req, res) {
    res.render('pages/index',{files : s.files});
});
